package ia.progetto.oggetti;

public interface Luogo {
	public boolean isPosabile(Oggetto o);
	public boolean isPrendibile(Oggetto o);
	public void posa(Oggetto o);
	public void prendi(Oggetto o);
	public boolean equals(Luogo l);
}
