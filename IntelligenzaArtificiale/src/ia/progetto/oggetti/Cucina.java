package ia.progetto.oggetti;

public class Cucina implements Luogo {
	public int spaghetti = 1;
	public int pizze = 1;
	public int bistecche = 10;

	public Cucina() {

	}

	public Cucina(Cucina c) {
		spaghetti=c.spaghetti;
		pizze=c.pizze;
		bistecche=c.bistecche;
	}

	@Override
	public boolean isPosabile(Oggetto o) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isPrendibile(Oggetto o) {
		if (o.equals(Cibo.BISTECCA)) {
			if(bistecche>0){
				bistecche --;
				return true;
			}
			return false;
		} else if (o.equals(Cibo.PIZZA)) {
			if(pizze>0){
				pizze --;
				return true;
			}
			return false;
		} else if (o.equals(Cibo.SPAGHETTI)) {
			if(spaghetti>0){
				spaghetti --;
				return true;
			}
			return false;
		}
		if(o.equals(Piatto.SPORCO) || o.equals(Posata.SPORCO)){
			return false;
		}
		
		return true;

	}

	@Override
	public void posa(Oggetto o) {
		if (o.equals(Cibo.BISTECCA)) {
			if(bistecche>0){
				bistecche ++;
				return ;
			}
		} else if (o.equals(Cibo.PIZZA)) {
			if(pizze>0){
				pizze ++;
				return ;
			}
		} else if (o.equals(Cibo.SPAGHETTI)) {
			if(spaghetti>0){
				spaghetti ++;
				return ;
			}
		}

	}

	@Override
	public void prendi(Oggetto o) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean equals(Luogo l) {
		// TODO Auto-generated method stub
		return false;
	}

}
