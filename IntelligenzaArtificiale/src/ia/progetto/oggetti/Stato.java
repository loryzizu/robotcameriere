package ia.progetto.oggetti;

import java.util.ArrayList;
import java.util.List;

public class Stato {
	
	public List<Tavolo> tavoli= new ArrayList<Tavolo>();

	public List<Oggetto> oggettiInMano=new ArrayList<Oggetto>();
	public int posizione=0;
	
	public static int cnt=0;
	public int id;
	public Stato statoPadre=null;
	
	private int pizze=2;
	private int bistecche=2;
	private int spaghetti=2;
	
	private List<Oggetto> tipoDiOggetto = new ArrayList<Oggetto>();
	private List<Oggetto> tipoDiOggettoDaPosare = new ArrayList<Oggetto>();
	
	public int costo=0;
	public  int currentLevel=0;
	
	
	public Stato(Tavolo t1, Tavolo t2,Tavolo t3){
		id=cnt;
		tavoli.add(0, t1);
		tavoli.add(1, t2);
		tavoli.add(2, t3);
	}
	
	public Stato(Stato s){
		cnt++;
		id=cnt;
		currentLevel=s.currentLevel+1;
		this.tavoli.add(0,new Tavolo((Tavolo)s.tavoli.get(0)));
		this.tavoli.add(1,new Tavolo((Tavolo)s.tavoli.get(1)));
		this.tavoli.add(2,new Tavolo((Tavolo)s.tavoli.get(2)));
		this.bistecche=s.getBistecche();
		this.pizze=s.getPizze();
		this.spaghetti=s.getSpaghetti();
		this.statoPadre=s;
	}
	
	
	
	public List<Oggetto> getTipoDiOggetto() {
		tipoDiOggetto = new ArrayList<Oggetto>();
		if(spaghetti>1){
			tipoDiOggetto.add(Cibo.SPAGHETTI);
		}
		if(pizze>1){
			tipoDiOggetto.add(Cibo.PIZZA);
		}
		if(bistecche>1){
			tipoDiOggetto.add(Cibo.BISTECCA);
		}
		tipoDiOggetto.add(Piatto.PULITO);
		tipoDiOggetto.add(Posata.PULITA);
		tipoDiOggetto.add(Piatto.SPORCO);
		tipoDiOggetto.add(Posata.SPORCA);
		
		return tipoDiOggetto;
	}
	
	public List<Oggetto> getTipoDiOggettoDaPosare() {
		tipoDiOggettoDaPosare = new ArrayList<Oggetto>();
		if(spaghetti>=1){
			tipoDiOggettoDaPosare.add(Cibo.SPAGHETTI);
		}
		if(pizze>=1){
			tipoDiOggettoDaPosare.add(Cibo.PIZZA);
		}
		if(bistecche>=1){
			tipoDiOggettoDaPosare.add(Cibo.BISTECCA);
		}
		tipoDiOggettoDaPosare.add(Piatto.PULITO);
		tipoDiOggettoDaPosare.add(Posata.PULITA);
		tipoDiOggettoDaPosare.add(Piatto.SPORCO);
		tipoDiOggetto.add(Posata.SPORCA);
		
		
		return tipoDiOggettoDaPosare;
	}

	public int getPizze() {
		return pizze;
	}

	public void setPizze(int pizze) {
		this.pizze = pizze;
	}

	public int getBistecche() {
		return bistecche;
	}

	public void setBistecche(int bistecche) {
		this.bistecche = bistecche;
	}

	public int getSpaghetti() {
		return spaghetti;
	}

	public void setSpaghetti(int spaghetti) {
		this.spaghetti = spaghetti;
	}

	
	
	 public boolean isGol(Stato stato) {
		if(stato.tavoli.get(0).equals(this.tavoli.get(0)) && 
				stato.tavoli.get(1).equals(this.tavoli.get(1)) && 
				stato.tavoli.get(2).equals(this.tavoli.get(2))){
			return true;
		}
		return false;
	}
	 public boolean equals(Stato stato){
		 if(stato.tavoli.get(0).equals(this.tavoli.get(0)) && 
					stato.tavoli.get(1).equals(this.tavoli.get(1)) && 
					stato.tavoli.get(2).equals(this.tavoli.get(2))){
				return true;
			}
			return false;
	 }
	 public void incrementa(Oggetto tipo){
		 if(tipo.equals(Cibo.BISTECCA)){
				bistecche++;
			}
			if(tipo.equals(Cibo.SPAGHETTI)){
				spaghetti++;
			}
			if(tipo.equals(Cibo.PIZZA)){
				pizze++;
			}
	 }
	 
	 public void decrementa(Oggetto tipo){
		 if(tipo.equals(Cibo.BISTECCA)){
				bistecche--;
			}
			if(tipo.equals(Cibo.SPAGHETTI)){
				spaghetti--;
			}
			if(tipo.equals(Cibo.PIZZA)){
				pizze--;
			}
	 }
	public void prendi(int i, Oggetto tipo){
		this.tavoli.get(i).prendi(tipo);
		
		incrementa(tipo);
	}
	public void posa(int i, Oggetto tipo){
		this.tavoli.get(i).posa(tipo);
		
		decrementa(tipo);
	}
	
	public void posa(int i, Oggetto tipo1,Oggetto tipo2){
		this.tavoli.get(i).posa(tipo1);
		this.tavoli.get(i).posa(tipo2);
		
		decrementa(tipo1);
		decrementa(tipo2);
	}
	
	public void prendi(int i, Oggetto tipo1,Oggetto tipo2){
		this.tavoli.get(i).prendi(tipo1);
		this.tavoli.get(i).prendi(tipo2);
		incrementa(tipo1);
		incrementa(tipo2);
	}
	
	
}
