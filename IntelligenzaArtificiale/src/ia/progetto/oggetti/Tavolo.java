package ia.progetto.oggetti;

import java.util.ArrayList;
import java.util.List;

public class Tavolo{
	private List<Oggetto> oggetti= new ArrayList<Oggetto>();
	
	
	
	public Tavolo(){
		
	}
	
	public Tavolo(Tavolo t){
		for (Oggetto oggetto : t.oggetti) {
			this.oggetti.add(oggetto);
		}
	}
	
	public List<Oggetto> getOggetti() {
		return oggetti;
	}
	public void setOggetti(List<Oggetto> oggetti) {
		this.oggetti = oggetti;
	}
	
	
	public boolean isPosabile(Oggetto o1, Oggetto o2) {
		if(this.oggetti.size()<2){//devo avere due spazi liberi per posare contemporaneamente una coppia
			if(isPosabile(o1) && isPosabile(o2)){
				return true;
			}
			
			if(isPosabile(o1) || isPosabile(o2)){
				if(!isPosabile(o1) && o1 instanceof Cibo && o2.equals(Piatto.PULITO)){
					return true;
				}
				if(!isPosabile(o2) && o2 instanceof Cibo && o1.equals(Piatto.PULITO)){
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	public boolean isPrendibile(Oggetto o1, Oggetto o2) {
		if(this.oggetti.size()>1){//devo avere almeno due oggetti per prendere cont una coppia
			if(isPrendibile(o1) && isPrendibile(o2)){
				return true;
			}
			
			//se � prendibile uno solo, controllo se quello non prendibile � piatto(ed � sul tavolo) e quello prendibile � cibo
			if(isPrendibile(o1) || isPrendibile(o2)){
				if(isPrendibile(o1) && o1 instanceof Cibo && o2 instanceof Piatto && isSulTavolo(o2)){
					return true;
				}
				if(isPrendibile(o2) && o2 instanceof Cibo && o1 instanceof Piatto && isSulTavolo(o1)){
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	
	public boolean isPosabile(Oggetto o) {
		
		if(!(oggetti.size() >=3)){
			for (Oggetto oggetto : oggetti) {
				if(o instanceof Posata && oggetto instanceof Posata ){
					return false;
				}
				if(o instanceof Piatto && oggetto instanceof Piatto ){
					return false;
				}
				if(o instanceof Cibo && oggetto instanceof Cibo ){
					return false;
				}
			}
			if(o instanceof Cibo){
				for (Oggetto oggetto2 : oggetti) {
					if(oggetto2.equals(Piatto.PULITO)){
						return true;
					}
				}
				return false;
			}
			return true;
		}
		return false;
	}
	
	public boolean isPrendibile(Oggetto o) {
		for (Oggetto oggetto : oggetti) {
			if(o.equals(oggetto)){
				if(o instanceof Piatto){
					for (Oggetto oggetto2 : oggetti) {
						if(oggetto2 instanceof Cibo){
							return false;
						}
					}
				}
				return true;
			}
		}
		return false;
	}
	
	
	public boolean isSulTavolo(Oggetto o) {
		for (Oggetto oggetto : oggetti) {
			if(o.equals(oggetto)){
				return true;
			}
		}
		return false;
	}
	
	public void posa(Oggetto o) {
		oggetti.add(o);
		
	}
	
	
	public void posa(Oggetto o1, Oggetto o2) {
		oggetti.add(o1);
		oggetti.add(o2);
	}
	
	public void prendi(Oggetto o) {
		oggetti.remove(o);
		
	}
	
	
	public void prendi(Oggetto o1, Oggetto o2) {
		oggetti.remove(o1);
		oggetti.remove(o2);
	}

	
	public boolean equals(Tavolo l) {
		if(l instanceof Tavolo){
			l=(Tavolo)l;
			List<Oggetto> oggettiGol=((Tavolo) l).oggetti;
			
			for (Oggetto oggettoGol : oggettiGol) {
				if(!oggetti.contains(oggettoGol)){
					return false;
				}
			}
			
			for (Oggetto oggetto : oggetti) {
				if(!oggettiGol.contains(oggetto)){
					return false;
				}
			}
			
			return true;
		}
		return false;
	}
	
	
}
