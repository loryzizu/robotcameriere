package ia.progetto.agenti;

import ia.progetto.oggetti.Cibo;
import ia.progetto.oggetti.Piatto;
import ia.progetto.oggetti.Posata;
import ia.progetto.oggetti.Stato;
import ia.progetto.oggetti.Tavolo;

public class ProprietarioRistorante {

	public static void main(String[] args) {
		//RobotCameriere robot= new RobotCameriereBFS();
		//RobotCameriere robot= new RobotCameriereDFS();
		//RobotCameriere robot= new RobotCameriereUCS();
		RobotCameriere robot= new RobotCameriereIDS();
		Tavolo t1=new Tavolo();
		Tavolo t2=new Tavolo();
		Tavolo t3=new Tavolo();
		int bistecche=3;
		int spaghetti=3;
		int pizze=3;
		robot.setStatoIniziale(new Stato(t1,t2,t3), bistecche, pizze, spaghetti);
	
		Tavolo t1f=new Tavolo();
		Tavolo t2f=new Tavolo();
		Tavolo t3f=new Tavolo();
		t1f.posa(Piatto.PULITO);
		t1f.posa(Cibo.BISTECCA);
		t1f.posa(Posata.PULITA);
		t2f.posa(Piatto.PULITO);
		t2f.posa(Cibo.BISTECCA);
		t2f.posa(Posata.PULITA);
		t3f.posa(Piatto.PULITO);
		t3f.posa(Cibo.PIZZA);
		t3f.posa(Posata.PULITA);
		robot.goal=new Stato(t1f,t2f,t3f);
		robot.start();

	}

}
