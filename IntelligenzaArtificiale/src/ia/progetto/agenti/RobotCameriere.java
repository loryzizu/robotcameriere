package ia.progetto.agenti;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ia.progetto.oggetti.Cibo;
import ia.progetto.oggetti.DifferenzeTraStati;
import ia.progetto.oggetti.Oggetto;
import ia.progetto.oggetti.Stato;
import ia.progetto.oggetti.Tavolo;

public class RobotCameriere extends ProprietarioRistorante {

	Stato goal;
	Stato stato;
	LinkedList<Stato> coda = new LinkedList<Stato>();
	LinkedList<Stato> codaAux = new LinkedList<Stato>();
	String risultato = "";

	public void setStatoIniziale(Stato s, int b, int sp, int p) {
		this.stato = s;
		for (Tavolo t : s.tavoli) {
			for (Oggetto o : t.getOggetti()) {
				if (o.equals(Cibo.BISTECCA)) {
					b++;
				} else if (o.equals(Cibo.PIZZA)) {
					p++;
				} else if (o.equals(Cibo.SPAGHETTI)) {
					sp++;
				}
			}
		}

		this.stato.setBistecche(b);
		this.stato.setPizze(p);
		this.stato.setSpaghetti(sp);
	}

	public void start() {

	}

	public String scriviStatiAttraversati(List<Stato> listaStatiSoluzione) {
		String result = "";
		for (Stato statoCorrente : listaStatiSoluzione) {
			result += "\n\tStato #" + statoCorrente.id + "";
			int i = 1;
			for (Tavolo tavolo : statoCorrente.tavoli) {
				result += "\n\t\tTavolo #" + i + ": ";
				int j = 1;
				for (Oggetto o : tavolo.getOggetti()) {
					result += "\n\t\t\tOggetto #" + j + ": "
							+ o.getClass().getSimpleName() + o.toString();
					j++;
				}
				i++;
			}
		}
		return result;
	}

	public void scriviSoluzione(Stato stato) {
		List<Stato> listaStatiSoluzione = new ArrayList<Stato>();
		Stato statoCorrente = stato;
		String result = "\n\n\n\n\n\n\n##################################################################################"
				+ "\n"
				+ "##################################################################################"
				+ "\n"
				+ "####################################SOLUZIONE#####################################"
				+ "\n"
				+ "##################################################################################"
				+ "\n"
				+ "##################################################################################\n\n"
				+ "Stati soluzione:";
		listaStatiSoluzione.add(0, statoCorrente);
		while (statoCorrente.statoPadre != null) {
			listaStatiSoluzione.add(0, statoCorrente.statoPadre);
			statoCorrente = statoCorrente.statoPadre;
		}
		String statiAttraversati = scriviStatiAttraversati(listaStatiSoluzione);
		result = result + statiAttraversati + "\n Profondit� della soluzione:"
				+stato.currentLevel
				+ "\n\n\n" + "AZIONI ROBOT:";

		LinkedList<Oggetto> mani = new LinkedList<Oggetto>();

		for (int i = 0; i < listaStatiSoluzione.size() - 1; i++) {
			Stato statoAttuale = listaStatiSoluzione.get(i);
			Stato figlio = listaStatiSoluzione.get(i + 1);
			DifferenzeTraStati differenze = findDifference(statoAttuale, figlio);
			String vadoInCucina = "";
			String posoOPrendo = "";
			String vadoTavolo = "";

			if (statoAttuale.posizione == differenze.tavolo) {
				if (i == 0) {
					vadoTavolo = "vado al tavolo" + (differenze.tavolo + 1);
				}
			} else {
				if (differenze.tavolo == 0) {
					vadoTavolo = " vado al tavolo 1";
				} else if (differenze.tavolo == 1) {
					vadoTavolo = " vado al tavolo 2";
				} else {
					vadoTavolo = " vado al tavolo 3";
				}
			}
			if (differenze.cambiamento
					.equals(DifferenzeTraStati.Cambiamento.POSATO)) {
				posoOPrendo = " poso";
				for (Oggetto oggetto2 : differenze.oggettiCambiatiSulTavolo) {
					posoOPrendo = posoOPrendo + " "
							+ oggetto2.getClass().getSimpleName() + " "
							+ oggetto2.toString();
				}
				for (Oggetto oggetto : differenze.oggettiCambiatiSulTavolo) {
					if (!mani.contains(oggetto)) {
						vadoInCucina = " vado in cucina ";
						vadoTavolo = " vado al tavolo "
								+ (differenze.tavolo + 1);
						
						if (mani.size() > 0) {
							vadoInCucina += "poso ";
							for (Oggetto oggettoMani : mani) {
								vadoInCucina = vadoInCucina
										+ " "
										+ oggettoMani.getClass()
												.getSimpleName() + " "
										+ oggettoMani.toString();
							}
							vadoInCucina += " e";
							mani.clear();// in cucina svuoto le mani
						}
						
						mani.addAll(differenze.oggettiCambiatiSulTavolo);
						vadoInCucina += " prendo ";
						for (Oggetto oggettoDaP : differenze.oggettiCambiatiSulTavolo) {
							vadoInCucina = vadoInCucina + " "
									+ oggettoDaP.getClass().getSimpleName()
									+ " " + oggettoDaP.toString();
						}
						break;
					}

				}

				mani.removeAll(differenze.oggettiCambiatiSulTavolo);

			} else {
				posoOPrendo = " prendo";
				for (Oggetto oggetto : differenze.oggettiCambiatiSulTavolo) {
					posoOPrendo = posoOPrendo + " "
							+ oggetto.getClass().getSimpleName() + " "
							+ oggetto.toString();
				}

				if ((2 - mani.size() < differenze.oggettiCambiatiSulTavolo
						.size())) {
					mani.clear();// in cucina svuoto le mani
					vadoInCucina = " vado in cucina, poso ";
					vadoTavolo = " vado al tavolo " + (differenze.tavolo + 1);
					for (Oggetto oggetto : mani) {
						vadoInCucina = vadoInCucina + " "
								+ oggetto.getClass().getSimpleName() + " "
								+ oggetto.toString();
						break;
					}

				}
				mani.addAll(differenze.oggettiCambiatiSulTavolo);
			}
			figlio.posizione = differenze.tavolo;
			result = result + "\n" + vadoInCucina + vadoTavolo + posoOPrendo;
		}
		this.risultato = result;
	}

	public DifferenzeTraStati findDifference(Stato statoAttuale, Stato figlio) {
		if (statoAttuale.equals(figlio)) {
			return null;
		}
		DifferenzeTraStati differenze = new DifferenzeTraStati();
		int i = 0;
		for (i = 0; i < 3; i++) {
			if (!statoAttuale.tavoli.get(i).equals(figlio.tavoli.get(i))) {
				differenze.tavolo = i;
				break;
			}
		}

		if (statoAttuale.tavoli.get(i).getOggetti().size() > figlio.tavoli
				.get(i).getOggetti().size()) {
			differenze.cambiamento = DifferenzeTraStati.Cambiamento.PRESO;
			for (Oggetto oggettoTavoloAttuale : statoAttuale.tavoli.get(i)
					.getOggetti()) {
				if (!figlio.tavoli.get(i).getOggetti()
						.contains(oggettoTavoloAttuale)) {// se l'oggetto
															// corrente dello
															// stato padre non
															// c'� nel figlio
					differenze.oggettiCambiatiSulTavolo
							.add(oggettoTavoloAttuale);
				}
			}
		} else {
			differenze.cambiamento = DifferenzeTraStati.Cambiamento.POSATO;
			for (Oggetto oggettoTavoloFiglio : figlio.tavoli.get(i)
					.getOggetti()) {
				if (!statoAttuale.tavoli.get(i).getOggetti()
						.contains(oggettoTavoloFiglio)) {
					differenze.oggettiCambiatiSulTavolo
							.add(oggettoTavoloFiglio);
				}
			}
		}
		return differenze;
	}

	public void infoStato(Stato stato) {
		List<Tavolo> tavoli = new ArrayList<Tavolo>();
		Tavolo tavolo1 = (Tavolo) stato.tavoli.get(0);
		Tavolo tavolo2 = (Tavolo) (Tavolo) stato.tavoli.get(1);
		Tavolo tavolo3 = (Tavolo) stato.tavoli.get(2);
		tavoli.add(tavolo1);
		tavoli.add(tavolo2);
		tavoli.add(tavolo3);

		// List<Oggetto> oggettiInMano = stato.oggettiInMano;

		try {
			Thread.sleep(1); // 1000 milliseconds is one second. }
		} catch (InterruptedException ex) {
			Thread.currentThread().interrupt();
		}

		System.out.println("Sono sullo stato#" + stato.id);
		for (Tavolo t : tavoli) {
			String st = "TAVOLO: ";
			for (Oggetto oggetto : t.getOggetti()) {
				st += oggetto.getClass().getSimpleName() + " "
						+ oggetto.toString() + " ";
			}
			System.out.println(st);
		}
		/*
		 * String oggettiInManoSt = "MANI: "; for (Oggetto oggetto :
		 * oggettiInMano) { oggettiInManoSt += oggetto.getClass().getName() +
		 * " " + oggetto.toString() + "  ...."; }
		 * 
		 * System.out.println(oggettiInManoSt);
		 */
	}

	public boolean isGoal() {
		return this.goal.isGol(stato);
	}

	public Stato getGoal() {
		return goal;
	}

}
