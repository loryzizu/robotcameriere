package ia.progetto.agenti;

import ia.progetto.oggetti.Oggetto;
import ia.progetto.oggetti.Stato;
import ia.progetto.oggetti.Tavolo;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public class RobotCameriereUCS extends RobotCameriere {

	public void start() {
		risultato="SOLUZIONE NON TROVATA";
		UCS();
		System.out.println(risultato);
	}

	private void UCS() {

		infoStato(stato);

		if (isGoal()) {
			scriviSoluzione(stato);
			return;
		} else {
			LinkedList<Stato> figliStatoTemp = new LinkedList<Stato>();
			figliStatoTemp.addAll(generaStatiFigli(stato));
			coda.addAll(figliStatoTemp);
			codaAux.addAll(figliStatoTemp);
			try {
				stato = coda.pop();
			} catch (NoSuchElementException e) {
				return;
			}
			UCS();
		}
	}
	public void accodaFiglio(Stato statoFiglio, int costo){
		//costo=statoFiglio.currentLevel;
		for (Stato statoCoda : codaAux) {
			if(statoFiglio.equals(statoCoda)){
				return;
			}
		}
		codaAux.add(statoFiglio);
		statoFiglio.costo=costo;
		if(coda.size()==0){
			coda.add(statoFiglio);
			return;
		}
		if(coda.size()==1){
			int costoAtt=coda.get(0).costo;
			if(costoAtt<=costo)
				coda.add(statoFiglio);
			else
				coda.add(0,statoFiglio);
			return;
		}
		if(coda.get(0).costo>costo){
			coda.add(0,statoFiglio);
			return;
		}
		for(int i=0; i<coda.size();i++){
			if(i==coda.size()-1){
				coda.add(statoFiglio);
				break;
			}
			int costoAtt=coda.get(i).costo;
			int costoSucc=coda.get(i+1).costo;
			if(costo>=costoAtt && costoSucc>costo){
				coda.add(i+1,statoFiglio);
				return;
			}
			
		}
		
		
		
	}
	public LinkedList<Stato> generaStatiFigli(Stato s) {
		LinkedList<Stato> result = new LinkedList<Stato>();
		List<Oggetto> listaOggetti = s.getTipoDiOggetto();
		List<Oggetto> listaOggettiDaPosare = s.getTipoDiOggettoDaPosare();
		
		for (int indiceTavolo = 0; indiceTavolo < 3; indiceTavolo++) {// PER OGNI TAVOLO
			Tavolo t= s.tavoli.get(indiceTavolo);
			if (indiceTavolo== 0) {
				System.out.println("Tavolo1:");
			} else if (indiceTavolo == 1) {
				System.out.println("Tavolo2:");
			} else if (indiceTavolo == 2) {
				System.out.println("Tavolo3:");
			}
		
	
			for (Oggetto tipo : listaOggettiDaPosare) {
				if (t.isPosabile(tipo)) {
					Stato statoFiglio = new Stato(stato);
					statoFiglio.statoPadre = stato;
					statoFiglio.posa(indiceTavolo, tipo);
					int costo=1;
					accodaFiglio(statoFiglio, costo);
					
					System.out.println("Stato" + s.id + " accoda figlio #" +
					statoFiglio.id + ", posando oggetto " +
					tipo.getClass().getSimpleName() + tipo);
					
				}
			}
			
			/*
			 * Per ogni coppia di oggetti, la poso se � posabile
			 */
			
				for (int i = 0; i < listaOggettiDaPosare.size(); i++) {
					Oggetto tipo1 = listaOggettiDaPosare.get(i);
					for (int j = i + 1; j < listaOggettiDaPosare.size(); j++) {
						Oggetto tipo2 = listaOggettiDaPosare.get(j);
						if (!tipo1.getClass().equals(tipo2.getClass())) {
							if (t.isPosabile(tipo1, tipo2)) {
								Stato statoFiglio = new Stato(stato);
								statoFiglio.statoPadre = stato;
								statoFiglio.posa(indiceTavolo, tipo1, tipo2);
								int costo=0;
								accodaFiglio(statoFiglio, costo);
								
								System.out.println("Stato" + s.id +
								" accoda figlio #" + statoFiglio.id +
								", posando oggetti " +
								tipo1.getClass().getSimpleName() + tipo1 + "..E.." +
								tipo2.getClass().getSimpleName() + tipo2);
								
		
							}
						}
					}
				}
	
			
	
			for (Oggetto tipo : listaOggetti) {
				if (t.isPrendibile(tipo)) {
					Stato statoFiglio = new Stato(stato);
					statoFiglio.statoPadre = stato;
					statoFiglio.prendi(indiceTavolo, tipo);
					int costo=4;
					accodaFiglio(statoFiglio, costo);
					 System.out.println("Stato" + s.id + " accoda figlio #"
					 + statoFiglio.id + ", prendendo oggetto "
					 + tipo.getClass().getSimpleName() + tipo);
				}
			}
			/*
			 * Per ogni coppia di oggetti, la prendo se � prendibile
			 */
			for (int i = 0; i < listaOggetti.size(); i++) {
				Oggetto tipo1 = listaOggetti.get(i);
				for (int j = i + 1; j < listaOggetti.size(); j++) {
					Oggetto tipo2 = listaOggetti.get(j);
					if (!tipo1.getClass().equals(tipo2.getClass())) {
						if (t.isPrendibile(tipo1, tipo2)) {
							Stato statoFiglio = new Stato(stato);
							statoFiglio.statoPadre = stato;
							statoFiglio.prendi(indiceTavolo, tipo1, tipo2);
							int costo=3;
							accodaFiglio(statoFiglio, costo);
							 System.out.println("Stato" + s.id +
							 " accoda figlio #"
							 + statoFiglio.id + ", prendendo oggetti "
							 + tipo1.getClass().getSimpleName() + tipo1
							 + "..E.." + tipo2.getClass().getSimpleName()
							 + tipo2);
	
						}
					}
				}
			}
		}
		return result;

	}



}
