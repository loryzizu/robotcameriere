package ia.progetto.agenti;

import java.util.List;

import ia.progetto.oggetti.Oggetto;
import ia.progetto.oggetti.Stato;
import ia.progetto.oggetti.Tavolo;

public class RobotCameriereDFS extends RobotCameriere {

	public void start(){
		risultato="SOLUZIONE NON TROVATA";
		DFS();
		System.out.println(risultato);
	}
	private boolean DFS() {

		infoStato(stato);

		if (isGoal()) {
			scriviSoluzione(stato);
			return true;
		} else {
			if(generaStatiFigli(stato))
				return true;
		}
		return false;
	}
	public boolean generaStatiFigli(Stato s) {
		List<Oggetto> listaOggetti = s.getTipoDiOggetto();
		List<Oggetto> listaOggettiDaPosare = s.getTipoDiOggettoDaPosare();
		
		for (int indiceTavolo = 0; indiceTavolo < 3; indiceTavolo++) {// PER OGNI TAVOLO
			Tavolo t= s.tavoli.get(indiceTavolo);
			
		/*
		 * Per ogni coppia di oggetti, la poso se � posabile
		 */
		
			for (int i = 0; i < listaOggettiDaPosare.size(); i++) {
				Oggetto tipo1 = listaOggettiDaPosare.get(i);
				for (int j = i + 1; j < listaOggettiDaPosare.size(); j++) {
					Oggetto tipo2 = listaOggettiDaPosare.get(j);
					if (!tipo1.getClass().equals(tipo2.getClass())) {
						if (t.isPosabile(tipo1, tipo2)) {
							Stato statoFiglio = new Stato(stato);
							statoFiglio.statoPadre = stato;
							statoFiglio.posa(indiceTavolo, tipo1, tipo2);
							boolean ceGia=false;
							for (Stato statoCoda : codaAux) {
								if(statoCoda.equals(statoFiglio)){
									ceGia=true;
								}
							}
							if(!ceGia){
								codaAux.add(statoFiglio);
								this.stato=statoFiglio;
								if(DFS()){
									return true;
								}
								this.stato=s;
								
							}
							
							/*
							 * System.out.println("Stato" + s.id +
							 * " accoda figlio #" + statoFiglio.id +
							 * ", posando oggetti " +
							 * tipo1.getClass().getSimpleName() + tipo1 + "..E.." +
							 * tipo2.getClass().getSimpleName() + tipo2);
							 */
	
						}
					}
				}
			}
	
			for (Oggetto tipo : listaOggettiDaPosare) {
				if (t.isPosabile(tipo)) {
					Stato statoFiglio = new Stato(stato);
					statoFiglio.statoPadre = stato;
					statoFiglio.posa(indiceTavolo, tipo);
					boolean ceGia=false;
					for (Stato statoCoda : codaAux) {
						if(statoCoda.equals(statoFiglio)){
							ceGia=true;
						}
					}
					if(!ceGia){
						codaAux.add(statoFiglio);
						this.stato=statoFiglio;
						if(DFS()){
							return true;
						}
						this.stato=s;
						
					}
					/*
					 * System.out.println("Stato" + s.id + " accoda figlio #" +
					 * statoFiglio.id + ", posando oggetto " +
					 * tipo.getClass().getSimpleName() + tipo);
					 */
				}
			}
	
			/*
			 * Per ogni coppia di oggetti, la prendo se � prendibile
			 */
			for (int i = 0; i < listaOggetti.size(); i++) {
				Oggetto tipo1 = listaOggetti.get(i);
				for (int j = i + 1; j < listaOggetti.size(); j++) {
					Oggetto tipo2 = listaOggetti.get(j);
					if (!tipo1.getClass().equals(tipo2.getClass())) {
						if (t.isPrendibile(tipo1, tipo2)) {
							Stato statoFiglio = new Stato(stato);
							statoFiglio.statoPadre = stato;
							statoFiglio.prendi(indiceTavolo, tipo1, tipo2);
							boolean ceGia=false;
							for (Stato statoCoda : codaAux) {
								if(statoCoda.equals(statoFiglio)){
									ceGia=true;
								}
							}
							if(!ceGia){
								codaAux.add(statoFiglio);
								this.stato=statoFiglio;
								if(DFS()){
									return true;
								}
								this.stato=s;
								
							}
							// System.out.println("Stato" + s.id +
							// " accoda figlio #"
							// + statoFiglio.id + ", prendendo oggetti "
							// + tipo1.getClass().getSimpleName() + tipo1
							// + "..E.." + tipo2.getClass().getSimpleName()
							// + tipo2);
	
						}
					}
				}
			}
	
			for (Oggetto tipo : listaOggetti) {
				if (t.isPrendibile(tipo)) {
					Stato statoFiglio = new Stato(stato);
					statoFiglio.statoPadre = stato;
					statoFiglio.prendi(indiceTavolo, tipo);
					boolean ceGia=false;
					for (Stato statoCoda : codaAux) {
						if(statoCoda.equals(statoFiglio)){
							ceGia=true;
						}
					}
					if(!ceGia){
						codaAux.add(statoFiglio);
						this.stato=statoFiglio;
						if(DFS()){
							return true;
						}
						this.stato=s;
						
					}
					// System.out.println("Stato" + s.id + " accoda figlio #"
					// + statoFiglio.id + ", prendendo oggetto "
					// + tipo.getClass().getSimpleName() + tipo);
				}
			}
		}
		return false;
	}
	


}
